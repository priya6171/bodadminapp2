package com.example.bodadminapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.bodadminapp.R;
import com.example.bodadminapp.adapter.AgendaDetailTabAdapter;
import com.example.bodadminapp.adapter.MeetingDetailsTabAdapter;
import com.example.bodadminapp.fragment.AgendaListFragment;
import com.example.bodadminapp.fragment.UpdateAgendaFragment;
import com.example.bodadminapp.fragment.UpdateMeetingDetailsFragment;
import com.example.bodadminapp.fragment.ViewAgendaFragment;
import com.example.bodadminapp.fragment.ViewMeetingDetailsFragment;
import com.google.android.material.tabs.TabLayout;

public class AgendaActivity extends AppCompatActivity {

    private TabLayout ty_agendadetails;
    private ViewPager vp_agendareplace;

    private AgendaDetailTabAdapter agendaDetailsTabAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);
        initController();
    }

    private void initController() {
        ty_agendadetails = (TabLayout) findViewById(R.id.agendadetails);
        vp_agendareplace = (ViewPager) findViewById(R.id.agendareplace);

        agendaDetailsTabAdapter = new AgendaDetailTabAdapter(getSupportFragmentManager());
        agendaDetailsTabAdapter.addFragment(new AgendaListFragment(), " Agenda List");
        agendaDetailsTabAdapter.addFragment(new UpdateAgendaFragment(), "Update Agenda");
        agendaDetailsTabAdapter.addFragment(new ViewAgendaFragment(), "View Agenda");

        vp_agendareplace.setAdapter(agendaDetailsTabAdapter);
        ty_agendadetails.setupWithViewPager(vp_agendareplace);
    }
}
