package com.example.bodadminapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.bodadminapp.R;
import com.example.bodadminapp.adapter.AgendaDetailTabAdapter;
import com.example.bodadminapp.fragment.AgendaListFragment;
import com.example.bodadminapp.fragment.TaskassignOfficerFragment;
import com.example.bodadminapp.fragment.TaskinMeetingFragment;
import com.example.bodadminapp.fragment.UpdateAgendaFragment;
import com.example.bodadminapp.fragment.ViewAgendaFragment;
import com.google.android.material.tabs.TabLayout;

public class TaskActivity extends AppCompatActivity {

    private TabLayout ty_taskdetails;
    private ViewPager vp_taskreplace;

    private AgendaDetailTabAdapter agendaDetailsTabAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        initController();
    }

    private void initController() {
        ty_taskdetails= (TabLayout) findViewById(R.id.taskdetails);
        vp_taskreplace= (ViewPager) findViewById(R.id.taskreplace);

        agendaDetailsTabAdapter = new AgendaDetailTabAdapter(getSupportFragmentManager());
        agendaDetailsTabAdapter.addFragment(new TaskinMeetingFragment(), " Task List");
        agendaDetailsTabAdapter.addFragment(new TaskassignOfficerFragment(), "Task Assign Officers");

        vp_taskreplace.setAdapter(agendaDetailsTabAdapter);
        ty_taskdetails.setupWithViewPager(vp_taskreplace);
    }
}

