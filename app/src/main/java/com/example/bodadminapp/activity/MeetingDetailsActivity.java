package com.example.bodadminapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.bodadminapp.R;
import com.example.bodadminapp.adapter.MeetingDetailsTabAdapter;
import com.example.bodadminapp.fragment.UpdateMeetingDetailsFragment;
import com.example.bodadminapp.fragment.ViewMeetingDetailsFragment;
import com.google.android.material.tabs.TabLayout;

public class MeetingDetailsActivity extends AppCompatActivity {

    private TabLayout ty_meetingdetails;
    private ViewPager vp_pagereplacemeeting;

    private MeetingDetailsTabAdapter meetingDetailsTabAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_details);

        initController();
    }

    private void initController() {
        ty_meetingdetails = (TabLayout) findViewById(R.id.meetingdetails);
        vp_pagereplacemeeting = (ViewPager) findViewById(R.id.pagereplacemeeting);

        meetingDetailsTabAdapter = new MeetingDetailsTabAdapter(getSupportFragmentManager());
        meetingDetailsTabAdapter.addFragment(new UpdateMeetingDetailsFragment(), "Update Details");
        meetingDetailsTabAdapter.addFragment(new ViewMeetingDetailsFragment(), "View Meeting Details");
       

        vp_pagereplacemeeting.setAdapter(meetingDetailsTabAdapter);
        ty_meetingdetails.setupWithViewPager(vp_pagereplacemeeting);
    }
}
