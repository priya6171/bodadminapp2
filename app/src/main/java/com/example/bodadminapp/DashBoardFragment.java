package com.example.bodadminapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.bodadminapp.activity.AgendaActivity;
import com.example.bodadminapp.activity.AttendanceActivity;
import com.example.bodadminapp.activity.DocumentationActivity;
import com.example.bodadminapp.activity.ExternalPropertyActivity;
import com.example.bodadminapp.activity.ListofPropertyActivity;
import com.example.bodadminapp.activity.MeetingDetailsActivity;
import com.example.bodadminapp.activity.TaskActivity;

public class DashBoardFragment extends Fragment implements View.OnClickListener {
    private View view;
    private Context mContext;
    private LinearLayout lnr_meetingDetails;
    private LinearLayout lnr_agenda;
    private  LinearLayout lnr_task;
    private LinearLayout lnr_attendance;
    private LinearLayout lnr_eproperty;
    private  LinearLayout lnr_doc;
    private LinearLayout lnr_listprop;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dashboardfragment, null);
        mContext = getActivity();
        initController();
        return view;
    }

    private void initController() {

        lnr_meetingDetails = view.findViewById(R.id.lnr_meetingDetails);
        lnr_agenda = view.findViewById(R.id.lnr_agenda);
        lnr_task = view.findViewById(R.id.lnr_task);
        lnr_attendance =view.findViewById(R.id.lnr_attendance);
        lnr_doc = view.findViewById(R.id.lnr_doc);
        lnr_eproperty = view.findViewById(R.id.lnr_eproperty);
        lnr_listprop = view.findViewById(R.id.lnr_listprop);
        lnr_meetingDetails.setOnClickListener(this);
        lnr_agenda.setOnClickListener(this);
        lnr_task.setOnClickListener(this);
        lnr_attendance.setOnClickListener(this);
        lnr_eproperty.setOnClickListener(this);
        lnr_doc.setOnClickListener(this);
        lnr_listprop.setOnClickListener(this);

    }
    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_layout, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.lnr_meetingDetails){

//            replaceFragment(new AgendaDocumentation_Fragment());

            startActivity(new Intent(getActivity(), MeetingDetailsActivity.class));
        }else
        if (v.getId() == R.id.lnr_agenda){

//            replaceFragment(new AgendaDocumentation_Fragment());

            startActivity(new Intent(getActivity(), AgendaActivity.class));
        }else
        if (v.getId() == R.id.lnr_task){

//            replaceFragment(new AgendaDocumentation_Fragment());

            startActivity(new Intent(getActivity(), TaskActivity.class));
        }else
        if (v.getId() == R.id.lnr_attendance){

//            replaceFragment(new AgendaDocumentation_Fragment());

            startActivity(new Intent(getActivity(), AttendanceActivity.class));
        }else
        if (v.getId() == R.id.lnr_eproperty){

//            replaceFragment(new AgendaDocumentation_Fragment());

            startActivity(new Intent(getActivity(), ListofPropertyActivity.class));
        }
        else
        if (v.getId() == R.id.lnr_doc){

//            replaceFragment(new AgendaDocumentation_Fragment());

            startActivity(new Intent(getActivity(), DocumentationActivity.class));
        }
        else
        if (v.getId() == R.id.lnr_listprop){

//            replaceFragment(new AgendaDocumentation_Fragment());

            startActivity(new Intent(getActivity(), ExternalPropertyActivity.class));
        }
    }
}
